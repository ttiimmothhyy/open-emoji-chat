import {Knex} from "knex";

export class InviteService {
    constructor(private knex: Knex) {}

    async sent(userId: number) {
        let existFriends: Array<number> = await this.knex
            .select("friend_id")
            .from("friends")
            .where("user_id", userId)
            .pluck("friend_id");

        let liked: Array<number> = await this.knex
            .select("target_user_id")
            .from("like_pages")
            .where("answer_user_id", userId)
            .pluck("target_user_id");

        return await this.knex
            .select("id", "display_name", "gender", "date_of_birth", "description", "icon")
            .from("users")
            .whereNotIn("id", existFriends)
            .whereIn("id", liked);
    }

    async received(userId: number) {
        let existFriends: Array<number> = await this.knex
            .select("friend_id")
            .from("friends")
            .where("user_id", userId)
            .pluck("friend_id");

        let received: Array<number> = await this.knex
            .select("answer_user_id")
            .from("like_pages")
            .where("target_user_id", userId)
            .andWhere({like_the_user: true})
            .pluck("answer_user_id");

        return await this.knex
            .select("id", "display_name", "gender", "date_of_birth", "description", "icon")
            .from("users")
            .whereNotIn("id", existFriends)
            .whereIn("id", received);
    }
}
