import {Knex} from "knex";
import {logger} from "../lib/logger";
import {VerifyUser, User} from "../lib/model";
import {register_status} from "../lib/variables";

export class UserService {
    constructor(private knex: Knex) {}

    async createUser(username: string, hashedPassword: string, email: string, uuid: string): Promise<number[]> {
        const user = {
            username: username,
            password: hashedPassword,
            email: email,
            register_status: register_status.to_verify,
            uuid: uuid,
        };
        logger.debug(user);
        return await this.knex.insert(user).into("users").returning("id");
    }

    async insert(user_id: number, username: string, password: string) {
        await this.knex.insert({user_id: user_id, username: username, password: password}).into("passwords");
    }

    async checkUuid(uuidInfo: VerifyUser) {
        return await this.knex
            .select("*")
            .from("users")
            .where("uuid", uuidInfo.uuid)
            .andWhere("username", uuidInfo.username);
    }

    async verifyUser(user: VerifyUser) {
        await this.knex("users").update(user).where("username", user.username);
    }

    async getUser(username: string) {
        return await this.knex.select("*").from("users").where("username", username);
    }

    async getEmail(email: string) {
        return await this.knex.select("*").from("users").where("email", email);
    }

    async login(username: string) {
        return await this.knex.select<Array<User>>("*").from("users").where("username", username);
    }

    async socialLogin(userKey: string) {
        return await this.knex.select("*").from("users").where("users.username", userKey);
    }

    async newSocialLogin(username: string, password: string, uuid: number) {
        return await this.knex.insert({username: username, password: password, uuid: uuid}).into("users").returning("id");
        logger.debug("user");
    }

    async getUsername(uuid: number) {
        return await this.knex.select("username").from("users").where("uuid", uuid);
    }

    async getAllUsers() {
        return await this.knex.select("*").from("users");
    }

    async resetPassword(username: string, hashedPassword: string, uuid: number) {
        await this.knex("users").update({password: hashedPassword, uuid: uuid}).where({username: username});
    }
}
