let uploadImageBtn = document.querySelector("#output_image");
const updateProfile = document.querySelector("#update-profile");
const updateError = document.querySelector(".error");
const hobbyArray = document.querySelectorAll(".hobby");

function preview_image(event) {
    let reader = new FileReader();
    reader.onload = function () {
        let output = document.getElementById("output_image");
        output.src = reader.result;
    };
    reader.readAsDataURL(event.currentTarget.files[0]);
}

uploadImageBtn.addEventListener("click", function () {
    document.querySelector("#original-upload-btn").click();
});

document.querySelector(".set-savor").addEventListener("click", function () {
    setSavor();
});

function setSavor() {
    document.querySelector(".set-savor").classList.add("display-none");
    document.querySelector(".savor").classList.remove("display-none");
}

async function getProfile() {
    const res = await fetch("/getProfile");
    const profile = await res.json();

    document.querySelector(".user-icon").src = `../uploads/${profile.icon}`;
    document.querySelector(".username").innerHTML = /*html*/ `${profile.username}`;

    switch (profile.gender) {
        case 1:
            document.querySelector(".gender").innerHTML = /*html*/ `<i class="fas fa-mars"></i>`;
            break;
        case 2:
            document.querySelector(".gender").innerHTML = /*html*/ `<i class="fas fa-venus"></i>`;
            break;
        case 3:
            document.querySelector(".gender").innerHTML = /*html*/ `<i class="fas fa-transgender"></i>`;
            break;
    }

    document.querySelector(".age").innerHTML = /*html*/ `${2021 - new Date(profile.date_of_birth).getFullYear()}`;
    document.querySelector(".display_name").value = profile.display_name;
    document.querySelector(".description").value = profile.description;
    document.querySelector(".orientation").value = profile.orientation;
    document.querySelector(".region").value = profile.region;
    document.querySelector(".height").value = profile.height;
    document.querySelector(".body_type").value = profile.body_type;
    document.querySelector(".job").value = profile.job;
}

async function loadHobbyData() {
    const res = await fetch("/hobbyList");
    const result = await res.json();
    for (let i = 0; i < result.length; i++) {
        hobbyArray[result[i].hobby_id].setAttribute("checked", true);
    }
}

updateProfile.addEventListener("submit", async function (event) {
    event.preventDefault();
    if (updateProfile.password.value !== updateProfile.confirmPassword.value) {
        updateError.innerHTML = /*html*/ `<div class="sign-up-error">請確認密碼</div>`;
        return;
    }

    const form = event.currentTarget;
    const formData = new FormData();
    formData.append("display_name", form.display_name.value);
    formData.append("description", form.description.value);

    if (form.password.value.length > 0) {
        formData.append("password", form.password.value);
    }
    if (form.orientation.value.length > 0) {
        formData.append("orientation", form.orientation.value);
    }
    formData.append("region", form.region.value);
    if (form.height.value.length > 0) {
        formData.append("height", form.height.value);
    }
    if (form.body_type.value.length > 0) {
        formData.append("body_type", form.body_type.value);
    }
    if (form.job.value.length > 0) {
        formData.append("job", form.job.value);
    }
    if (form.icon.files[0]) {
        formData.append("icon", form.icon.files[0]);
    }
    const res = await fetch("/updateProfileBasic", {
        method: "post",
        body: formData, // formData唔需要Content-Type 果個header
    });

    const hobbySend = [];
    for (let i = 0; i < hobbyArray.length; i++) {
        if (hobbyArray[i].checked) {
            hobbySend.push(i);
        }
    }
    await fetch("/updateProfileHobby", {
        method: "post",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify({hobby: hobbySend}),
    });
    if (res.status === 200) {
        document.querySelector(".successUpdate").innerHTML = /*html*/ `更新成功✅`;
        setTimeout(function () {
            window.location.href = "profile.html";
        }, 500);
    }
});

window.onload = async function () {
    await getProfile();
    await loadHobbyData();
};
