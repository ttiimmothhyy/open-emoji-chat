import {Request, Response} from "express";
import {logger} from "../lib/logger";
import {LikeService} from "../services/LikeService";
import fetch from "node-fetch";

export class LikeController {
    constructor(private likeService: LikeService) {}

    showUsers = async (req: Request, res: Response) => {
        try {
            let userID = req.session["user"].id;
            const targetID = parseInt(req.params.id);
            let targetUser = await this.likeService.targetUser(targetID, userID);
            res.json(targetUser);
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };

    sendRecording = async (req: Request, res: Response) => {
        try {
            let recordingSet = req.body;
            let userID = req.session["user"].id;
            const targetID = parseInt(req.params.id);
            if (req.file) {
                recordingSet = req.file.filename;
            }
            logger.debug(req.file.filename);
            await this.likeService.like(userID, targetID, recordingSet);
            res.json({message: recordingSet});
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };

    sendEmojiRecording = async (req: Request, res: Response) => {
        try {
            let recordingSet = req.body.message;
            let result = await fetch("http://localhost:7990/emojimodel", {
                method: "post",
                headers: {
                    "Content-type": "application/json",
                },
                body: JSON.stringify({message: recordingSet.substring(0, recordingSet.length - 4)}),
            });
            logger.debug(recordingSet.substring(0, recordingSet.length - 4));
            let message = await result.json();
            logger.debug(JSON.stringify(message, null, 4));
			if(message && message.emoji){
				await this.likeService.addEmoji(recordingSet, message.emoji);
			}
            res.json({success: true});
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };

    rejectTarget = async (req: Request, res: Response) => {
        try {
            let userID = req.session["user"].id;
            const targetID = parseInt(req.params.id);
            await this.likeService.reject(userID, targetID);
            res.json({success: true});
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };
}
