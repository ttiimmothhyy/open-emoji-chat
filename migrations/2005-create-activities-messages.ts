import {Knex} from "knex";

export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable("activities_messages", (table) => {
        table.increments();
        table.integer("activity_id").unsigned();
        table.foreign("activity_id").references("activities.id");
        table.integer("participant_id").unsigned();
        table.foreign("participant_id").references("users.id");
        // table.string("content_type").notNullable(); // audio or text
        table.string("content");
        table.timestamp("created_at").defaultTo(knex.fn.now());
    });
}

export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable("activities_messages");
}
