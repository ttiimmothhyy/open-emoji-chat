import {Knex} from "knex";

export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable("like_pages", (table) => {
        table.increments();
        table.integer("answer_user_id").unsigned();
        table.foreign("answer_user_id").references("users.id");
        table.integer("target_user_id").unsigned();
        table.foreign("target_user_id").references("users.id");
        table.boolean("like_the_user").notNullable();
        table.string("recording");
        table.string("emoji").defaultTo("emoji");
        table.timestamp("created_at").defaultTo(knex.fn.now());
    });
}

export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable("like_pages");
}
