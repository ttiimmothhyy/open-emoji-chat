import {v4 as uuid} from "uuid";
import {knex} from "../main";

export async function uuidGenerator(): Promise<any> {
    let id = uuid();
    const uuids = await knex.select("uuid").from("users");
    if (uuids[0]) {
        for (let individual of uuids) {
            if (individual.uuid === id) {
                return uuidGenerator();
            } else {
                return id;
            }
        }
    } else {
        return id;
    }
}
