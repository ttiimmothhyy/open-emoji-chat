export const register_status = Object.freeze({
    to_verify: 1,
    register: 2,
    delete: 3,
});
