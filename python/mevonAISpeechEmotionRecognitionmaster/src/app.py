#%%
from sanic import Sanic
from sanic.response import json
import tensorflow as tf
import numpy as np
import os
from speechEmotionRecognition import emojiModel

#%%
os.environ['KMP_DUPLICATE_LIB_OK'] = 'True'
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

app = Sanic("Emojimodel")

@app.post("/emojimodel")
async def call_model(request):
    messageEmoji=request.json
    result=emojiModel(messageEmoji["message"])
    return json({"emoji": result})

if __name__ == "__main__":
    print("Listening at http://localhost:7990")
    app.run(host="localhost", port=7990, debug=False, access_log=False)
